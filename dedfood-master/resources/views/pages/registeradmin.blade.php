@include('template.main')

<div class="container col-6 p-4 border border-2 border-primary rounded-3 my-auto mt-5">
    <h2>Halaman Register Admin</h2>
        @if (session()->has('unik'))
    <div class="alert alert-danger alert-dismissible fade show mt-3" role="alert">
        {{session('unik')}}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
    <form action="/registeradmin" method="post">
        @csrf
        <label for="username" class="form-label">Username</label>
        <input type="text" class="form-control" id="username" name="username">
        <label for="password" class="form-label">Password</label>
        <input type="password" class="form-control" id="password" name="password" minlength="8">
        <label for="nama" class="form-label">Nama</label>
        <input type="text" class="form-control" id="nama" name="nama">
        <label for="kontak" class="form-label">Kontak</label>
        <input type="text" class="form-control" id="kontak" name="kontak">
        <input type="submit" value="REGISTER" class="btn btn-primary mt-4">
    </form>
</div>
@include('template.footer')