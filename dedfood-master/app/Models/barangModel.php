<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class barangModel extends Model
{
    use HasFactory;
    protected $table ='barang';
    protected $fillable = [
        'nama_barang',
        'harga_barang',
        'supplier_id',
        'foto_barang',
        'stok',
    ];
}
